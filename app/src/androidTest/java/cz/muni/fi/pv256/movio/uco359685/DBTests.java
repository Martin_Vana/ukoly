package cz.muni.fi.pv256.movio.uco359685;

/**
 * Created by Martin on 11/30/2015.
 */

import android.database.Cursor;
import android.test.AndroidTestCase;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import cz.muni.fi.pv256.movio.uco359685.adapter.FilmDataSource;
import cz.muni.fi.pv256.movio.uco359685.model.Film;

public class DBTests extends AndroidTestCase {

    private static final String TAG = FilmDataSource.class.getSimpleName();

    private FilmDataSource mManager;

    @Override
    protected void setUp() throws Exception {
        Log.d(TAG, "building up");
        mManager = new FilmDataSource(mContext);
    }

    @Override
    public void tearDown() throws Exception {
        Log.d(TAG, "tearing down");
        ArrayList<Film> DBfilms = mManager.getAllFilms();
        for (Film film : DBfilms) {
            mManager.deleteFilm(film);
        }
    }

    public void testAddAndGetFilms() throws Exception {
        Film f1 = createFilm("test0", 0L);
        Film f2 = createFilm("test1", 1L);

        mManager.addFilm(f1);
        mManager.addFilm(f2);

        ArrayList<Film> DBfilms = mManager.getAllFilms();
        Log.i(TAG, DBfilms.toString());
        assertTrue(DBfilms.size() == 2);

        for (Film film : DBfilms) {
            mManager.deleteFilm(film);
        }
    }

    public void testDeleteFilm() throws Exception {

        Film f1 = createFilm("test0", 0L);
        Film f2 = createFilm("test1", 1L);

        mManager.addFilm(f1);
        mManager.addFilm(f2);

        ArrayList<Film> films2 = mManager.getAllFilms();
        Log.d(TAG, films2.toString());
        assertTrue(films2.size() == 2);

        mManager.deleteFilm(f1);
        mManager.deleteFilm(f2);

        ArrayList<Film> films0 = mManager.getAllFilms();
        Log.d(TAG, films0.toString());
        assertTrue(films0.size() == 0);
//        assertEquals(expectedWorkTimes, workTimes);
    }

    private Film createFilm(String name, Long id) {
        Film film = new Film(
                id, name, "", "", 0
        );
        return film;
    }
}