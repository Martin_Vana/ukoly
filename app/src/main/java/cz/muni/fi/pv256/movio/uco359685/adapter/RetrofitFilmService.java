package cz.muni.fi.pv256.movio.uco359685.adapter;

import java.util.ArrayList;

import cz.muni.fi.pv256.movio.uco359685.Constants;
import cz.muni.fi.pv256.movio.uco359685.model.Film;
import cz.muni.fi.pv256.movio.uco359685.model.GsonWrappper;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Martin on 11/19/2015.
 */
public interface RetrofitFilmService {

    @GET(value = Constants.POPULAR_URL + Constants.API_KEY)
    Call<GsonWrappper> getPopularMovies();

    @GET(value = Constants.KIDS_URL + Constants.API_KEY)
    Call<GsonWrappper> getKidsMovies();

    @GET(value = Constants.MOVIE_URL + Constants.API_KEY_ALT)
    Call<Film> getMovie(@Path("id") Long id);

}