package cz.muni.fi.pv256.movio.uco359685;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.opengl.Visibility;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import cz.muni.fi.pv256.movio.uco359685.adapter.FetchDataIntentService;
import cz.muni.fi.pv256.movio.uco359685.adapter.FilmDataSource;
import cz.muni.fi.pv256.movio.uco359685.adapter.FilmListAdapter;
import cz.muni.fi.pv256.movio.uco359685.adapter.FilmLoader;
import cz.muni.fi.pv256.movio.uco359685.model.Film;
import cz.muni.fi.pv256.movio.uco359685.model.Singleton;
import cz.muni.fi.pv256.movio.uco359685.updater.UpdaterSyncAdapter;

public class MainActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<ArrayList<Film>> {

    private boolean mTwoPane;
    public MainActivity mContext;
    private ResponseReceiver mReceiver;
    public static FilmListAdapter mAdapter;
    public static MainActivity self;
    public GridView mGridView;
    private boolean discover = true;

    public static FilmDataSource sFilmDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        self = this;
        super.onCreate(savedInstanceState);


        setContentView(R.layout.main_activity);

        mContext = this;

        if (BuildConfig.LOGGING)
            Log.i("", "main activity create");

        //are we doing two pane display?
        if (findViewById(R.id.film_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        // initialize the GridView
        mGridView = (GridView) findViewById(R.id.film_grid_view);
        mGridView.setOnItemClickListener(this);
        mAdapter = new FilmListAdapter(this);
        mGridView.setAdapter(mAdapter);
        mGridView.setEmptyView(findViewById(R.id.empty));

        sFilmDataSource = new FilmDataSource(this);
        getSupportLoaderManager().initLoader(0, null, this);
        //testSyncAddMovie();

        Intent fetchDataIntent = new Intent(MainActivity.this, FetchDataIntentService.class);
        startService(fetchDataIntent);

        checkConnection();

        UpdaterSyncAdapter.initializeSyncAdapter(this);

        if (BuildConfig.LOGGING)
            Log.i("", "main activity create end ------------------------------------------------");
    }

    @Override
    protected void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);

        super.onPause();
    }

    @Override
    protected void onResume() {

        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        mReceiver = new ResponseReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);

        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_discover) {
            if (BuildConfig.LOGGING)
                Log.i("", "Discover item selected");
            Intent fetchDataIntent = new Intent(MainActivity.this, FetchDataIntentService.class);
            startService(fetchDataIntent);

            discover = true;
        }
        if (id == R.id.action_favorites) {
            if (BuildConfig.LOGGING)
                Log.i("", "Favorites item selected");

            getSupportLoaderManager().restartLoader(0, null, this);

            discover = false;
        }
        if (id == R.id.action_sync) {

            UpdaterSyncAdapter.syncImmediately(this);
        }
        checkConnection();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putParcelable("film_id", Singleton.getInstance().getFilmList().get(position));
            FilmDetailFragment fragment = new FilmDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.film_detail_container, fragment)
                    .commit();

            if (BuildConfig.LOGGING)
                Log.i("", "Created fragment " + mTwoPane);

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, FilmDetailActivity.class);
            detailIntent.putExtra("film_id", Singleton.getInstance().getFilmList().get(position));
            startActivity(detailIntent);

            if (BuildConfig.LOGGING)
                Log.i("", "Created activity " + mTwoPane);
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public static void checkConnection() {
        if (!self.isNetworkConnected()) {
            ((TextView)self.findViewById(R.id.empty)).setText(R.string.no_connection);
        } else {
            ((TextView)self.findViewById(R.id.empty)).setText(R.string.empty_list);
        }
    }

    public static class ResponseReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP = "com.mamlambo.intent.action.MESSAGE_PROCESSED";

        public ResponseReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
//          way to send parameters
//            String text = intent.getStringExtra(FetchDataIntentService.PARAM_OUT_MSG);

            if (BuildConfig.LOGGING)
                Log.i("", "receivujem");

            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public Loader<ArrayList<Film>>onCreateLoader(int i, Bundle bundle) {
        if (BuildConfig.LOGGING)
            Log.i("", "create loader?");
        return new FilmLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Film>> loader, ArrayList<Film> data) {

        Singleton.getInstance().setFilmList(data);

        ArrayList<Singleton.Header> headers = new ArrayList<Singleton.Header>();
        Singleton.Header hFav = new Singleton.Header();
        hFav.Count = data.size();
        hFav.Text = getResources().getString(R.string.favorite);
        headers.add(hFav);
        Singleton.getInstance().setHeaderList(headers);

        if (BuildConfig.LOGGING)
            Log.i("", "vyzera to ze som naloadoval db");

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Film>> loader) {

    }

    public static void StaticReloadDB() {
        self.getSupportLoaderManager().restartLoader(0, null, self);
    }

    //method that adds a movie with fake data, to be synced and replaced with real data
    public void testSyncAddMovie() {

        Film test = new Film(550, "this should be fight club", "", "", 1);

        FilmDataSource manager = new FilmDataSource(mContext);
        if (manager.containsFilm(test)) {
            manager.deleteFilm(test);
        }
        manager.addFilm(test);
    }
}
