package cz.muni.fi.pv256.movio.uco359685.updater;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.muni.fi.pv256.movio.uco359685.BuildConfig;
import cz.muni.fi.pv256.movio.uco359685.Constants;
import cz.muni.fi.pv256.movio.uco359685.R;
import cz.muni.fi.pv256.movio.uco359685.adapter.FilmDataSource;
import cz.muni.fi.pv256.movio.uco359685.adapter.RetrofitFilmService;
import cz.muni.fi.pv256.movio.uco359685.model.Film;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Martin on 1/2/2016.
 */
public class UpdaterSyncAdapter extends AbstractThreadedSyncAdapter {

    // Interval at which to sync with the server, in seconds.
    public static final int SYNC_INTERVAL = 60 * 60 * 24; //day
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;

    public UpdaterSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    /**
     * Helper method to schedule the sync adapter periodic execution
     */
    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.sync_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder()
                    .syncPeriodic(syncInterval, flexTime)
                    .setSyncAdapter(account, authority)
                    .setExtras(Bundle.EMPTY) //enter non null Bundle, otherwise on some phones it crashes sync
                    .build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account, authority, Bundle.EMPTY, syncInterval);
        }
    }

    /**
     * Helper method to have the sync adapter sync immediately
     *
     * @param context The context used to access the account service
     */
    public static void syncImmediately(Context context) {
        String authority = context.getString(R.string.sync_authority);
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

        if (ContentResolver.isSyncPending(getSyncAccount(context), authority)  ||
                ContentResolver.isSyncActive(getSyncAccount(context), authority)) {
            if (BuildConfig.LOGGING)
                Log.i("ContentResolver", "SyncPending, canceling");
            ContentResolver.cancelSync(getSyncAccount(context), authority);
        }
        ContentResolver.requestSync(getSyncAccount(context), authority, bundle);
    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one if the
     * fake account doesn't exist yet.  If we make a new account, we call the onAccountCreated
     * method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(context.getString(R.string.app_name), context.getString(R.string.sync_type));

        // If the password doesn't exist, the account doesn't exist
        if (null == accountManager.getPassword(newAccount)) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            ContentResolver.setIsSyncable(newAccount, context.getString(R.string.sync_authority), 1);

            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {
        /*
         * Since we've created an account
         */
        UpdaterSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);

        /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.sync_authority), true);

        /*
         * Finally, let's do a sync to get things started
         */
        syncImmediately(context);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

        if (BuildConfig.LOGGING)
            Log.i("", "SYNCYNGGGGGGGGGGGG");

        FilmDataSource mManager = new FilmDataSource(getContext());
        ArrayList<Film> favs = mManager.getAllFilms();

        try {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Long.class, new JsonDeserializer<Long>() {
                        @Override
                        public Long deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                Date createDate = formatter.parse(json.getAsString());
                                return createDate.getTime();
                            } catch (java.text.ParseException e) {
                                throw new JsonParseException(e.getMessage());
                            }
                        }
                    })
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            RetrofitFilmService retrofitAPI = retrofit.create(RetrofitFilmService.class);

            for (Film f : favs) {
                Film temp = retrofitAPI.getMovie(f.getId()).execute().body();

                if (! f.equals(temp)) {
                    if (BuildConfig.LOGGING)
                        Log.i("", "movie id synced: " + f.getId());
                } else {
                    if (BuildConfig.LOGGING)
                        Log.i("", "movie id NOOOOOOOOOOT synced: " + f.getId());
                    mManager.deleteFilm(f);
                    mManager.addFilm(temp);
                }
            }
        }
        catch (Exception e)
        {

        }
    }
}