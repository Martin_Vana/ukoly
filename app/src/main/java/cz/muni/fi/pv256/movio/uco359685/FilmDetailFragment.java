package cz.muni.fi.pv256.movio.uco359685;

import android.graphics.Matrix;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Date;

import cz.muni.fi.pv256.movio.uco359685.adapter.FilmDataSource;
import cz.muni.fi.pv256.movio.uco359685.model.Film;

/**
 * Created by Martin on 10/25/2015.
 */
public class FilmDetailFragment  extends Fragment {

    /**
     * The dummy content this fragment is presenting.
     */
    private Film mItem;
    public static boolean add = true;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FilmDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey("film_id")) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = getArguments().getParcelable("film_id");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.film_detail_fragment,
                container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) view.findViewById(R.id.textView)).setText(mItem.getTitle());

            ((TextView) view.findViewById(R.id.dateTextView)).setText(getDateString(mItem));

            ImageView pic = ((ImageView) view.findViewById(R.id.imageView));
            Picasso.with(getContext()).load(Constants.BASE_POSTER_URL + mItem.getCoverPath()).into(pic);

            ImageView bg = ((ImageView) view.findViewById(R.id.image_bg));
            Picasso.with(getContext()).load(Constants.BASE_BACKDROP_URL + mItem.getBackground()).into(bg);

            //rotate button to make X from +
            FilmDataSource dbs = new FilmDataSource(getContext());
            if (dbs.containsFilm(mItem))
            {
                SetX(view);
            }

            FloatingActionButton button = (FloatingActionButton) view.findViewById(R.id.plus_button);
            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (BuildConfig.LOGGING)
                        Log.i("", "clikol som +");
                    FilmDataSource dbs = new FilmDataSource(getContext());
                    if (add) {
                        dbs.addFilm(mItem);
                        SetX(view);
                    }
                    else {
                        dbs.deleteFilm(mItem);
                        SetPlus(view);
                    }
                    MainActivity.StaticReloadDB();
                }
            });
        }

        return view;
    }

    private String getDateString(Film film) {
        Date relDate = new Date(film.getReleaseDate());
        return DateFormat.format("dd. MM. yyyy", relDate).toString();
    }
    public static void SetPlus(View view){
        FloatingActionButton plusButton = ((FloatingActionButton) view.findViewById(R.id.plus_button));
        plusButton.setRotation(0);

        add = true;
    }
    public static void SetX(View view) {
        FloatingActionButton plusButton = ((FloatingActionButton) view.findViewById(R.id.plus_button));
        plusButton.setRotation(45);

        add = false;
    }
}