package cz.muni.fi.pv256.movio.uco359685.adapter;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Martin on 11/29/2015.
 */
public class SQL extends SQLiteOpenHelper {

    public static final String TABLE_FAVORITES = "favorites";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_COVER = "cover";
    public static final String COLUMN_BACKGROUND = "background";
    public static final String COLUMN_RELEASE = "release";

    private static final String DATABASE_NAME = "favorites.db";
    private static final int DATABASE_VERSION = 1;

    public SQL(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        // Database creation sql statement
        final String DATABASE_CREATE =
                "CREATE TABLE "
                        + TABLE_FAVORITES + "("
                        + COLUMN_ID + " INTEGER PRIMARY KEY, "
                        + COLUMN_NAME + " TEXT, "
                        + COLUMN_COVER + " TEXT, "
                        + COLUMN_BACKGROUND + " TEXT, "
                        + COLUMN_RELEASE + " LONG"
                        + ");";

        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SQL.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVORITES);
        onCreate(db);
    }

}
