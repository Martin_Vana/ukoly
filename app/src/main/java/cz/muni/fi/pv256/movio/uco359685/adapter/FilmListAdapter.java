package cz.muni.fi.pv256.movio.uco359685.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import cz.muni.fi.pv256.movio.uco359685.BuildConfig;
import cz.muni.fi.pv256.movio.uco359685.Constants;
import cz.muni.fi.pv256.movio.uco359685.R;
import cz.muni.fi.pv256.movio.uco359685.model.Film;
import cz.muni.fi.pv256.movio.uco359685.model.Singleton;

/**
 * Created by Martin on 10/18/2015.
 */
public class FilmListAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter {

    private Context mContext;
    private ArrayList<Singleton.Header> getHeaderList() {
        return Singleton.getInstance().getHeaderList();
    }
    private ArrayList<Film> getFilmList() {
        return Singleton.getInstance().getFilmList();
    }

    public FilmListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return getFilmList().size();
    }

    @Override
    public Object getItem(int i) {
        return getFilmList().get(i);
    }

    @Override
    public long getItemId(int i) {
        return getFilmList().get(i).getId();
    }

    @Override
    public int getCountForHeader(int i) {
        return getHeaderList().get(i).Count;
    }

    @Override
    public int getNumHeaders() {
        return getHeaderList().size();
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    private static class ViewHolder {
        ImageView view;
    }
    private static class HeaderViewHolder {
        TextView header;
    }

    @Override
    public View getHeaderView(int i, View convertView, ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.film_header, parent, false);

            HeaderViewHolder holder = new HeaderViewHolder();
            holder.header = (TextView) convertView.findViewById(R.id.film_header);

            convertView.setTag(holder);

            if (BuildConfig.LOGGING)
                Log.i("", "inflate hlavicky " + i);
        }

        HeaderViewHolder holder = (HeaderViewHolder) convertView.getTag();
        holder.header.setText(getHeaderList().get(i).Text);

        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.film_item, parent, false);

            ViewHolder holder = new ViewHolder();
            holder.view = (ImageView) convertView.findViewById(R.id.film_item);

            convertView.setTag(holder);

            if (BuildConfig.LOGGING)
                Log.i("", "inflate riadku " + position);
        } else {
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();

        if (holder != null) {
            holder.view.setMinimumHeight((int) (holder.view.getWidth() * 1.5));

            Picasso.with(mContext).load(Constants.BASE_POSTER_URL + getFilmList().get(position).getCoverPath()).into(holder.view);
        }

        return convertView;
    }
}
