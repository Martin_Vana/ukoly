package cz.muni.fi.pv256.movio.uco359685.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Martin on 10/18/2015.
 */
public class Film implements Parcelable {

    @SerializedName("id")
    private long id;
    @SerializedName("poster_path")
    private String coverPath;
    @SerializedName("backdrop_path")
    private String background;
    @SerializedName("original_title")
    private String title;
    @SerializedName("release_date")
    private Long releaseDate;

    public long getId() {return id;}
    public long getReleaseDate() {
        return releaseDate;
    }
    public String getCoverPath() {
        return coverPath;
    }
    public String getBackground() {
        return background;
    }
    public String getTitle() {
        return title;
    }

    public static final Creator<Film> CREATOR = new Creator<Film>() {
        @Override
        public Film createFromParcel(Parcel in) {
            return new Film(in);
        }

        @Override
        public Film[] newArray(int size) {
            return new Film[size];
        }
    };

    public Film() {

    }
    public Film(long id, String t, String cp, String bg, long rd) {
        this.id = id;
        title = t;
        coverPath = cp;
        background = bg;
        releaseDate = rd;
    }
    protected Film(Parcel in) {
        id = in.readLong();
        title = in.readString();
        coverPath = in.readString();
        background = in.readString();
        releaseDate = in.readLong();
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public int describeContents() {
        return (int) id;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(coverPath);
        dest.writeString(background);
        dest.writeLong(releaseDate);
    }

    @Override
    public boolean equals(Object other) {
        boolean result = false;

        try {
            Film o = (Film) other;
            if ((o.getReleaseDate() != getReleaseDate()) &&
                (o.getCoverPath() != getCoverPath()) &&
                (o.getBackground() != getBackground()) &&
                (o.getTitle() != getTitle())) result = true;
        } catch (Exception e) {}

        return result;
    }
}
