package cz.muni.fi.pv256.movio.uco359685.model;

import java.util.ArrayList;

/**
 * Created by Martin on 11/6/2015.
 */
public class Singleton {
    private static Singleton mInstance = null;

    private ArrayList<Header> mHeaderList;
    private ArrayList<Film> mFilmList;

    private Singleton(){
    }

    public static Singleton getInstance(){
        if(mInstance == null)
        {
            mInstance = new Singleton();
        }
        return mInstance;
    }

    public ArrayList<Film> getFilmList() {
        if (mFilmList == null) return new ArrayList<Film>();
        return mFilmList;
    }

    public void setFilmList(ArrayList<Film> filmList) {
        mFilmList = filmList;
    }

    public ArrayList<Header> getHeaderList() {
        if (mHeaderList == null) return new ArrayList<Header>();
        return mHeaderList;
    }

    public void setHeaderList(ArrayList<Header> headerList) {
        mHeaderList = headerList;
    }

    public static class Header {
        public int Count = 0;
        public String Text = "";
        public Header() {}
    }
    public static Header getHeader(String text) {
        Header result = new Header();
        result.Text = text;
        return result;
    }
}
