package cz.muni.fi.pv256.movio.uco359685.model;

import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Martin on 11/7/2015.
 */
public class GsonWrappper {
    @SerializedName("page")
    public int page;

    @SerializedName("results")
    public ArrayList<Film> results;
}