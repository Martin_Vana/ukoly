package cz.muni.fi.pv256.movio.uco359685.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import cz.muni.fi.pv256.movio.uco359685.BuildConfig;
import cz.muni.fi.pv256.movio.uco359685.model.Film;

/**
 * Created by Martin on 11/29/2015.
 */
public class FilmDataSource {

    // Database fields
    private SQLiteDatabase database;
    private SQL dbHelper;
    private String[] allColumns = {
            SQL.COLUMN_ID,
            SQL.COLUMN_NAME,
            SQL.COLUMN_COVER,
            SQL.COLUMN_BACKGROUND,
            SQL.COLUMN_RELEASE };

    public FilmDataSource(Context context) {
        dbHelper = new SQL(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Film addFilm(Film toAdd) {

        open();

        ContentValues values = new ContentValues();
        values.put(SQL.COLUMN_ID, toAdd.getId());
        values.put(SQL.COLUMN_NAME, toAdd.getTitle());
        values.put(SQL.COLUMN_COVER, toAdd.getCoverPath());
        values.put(SQL.COLUMN_BACKGROUND, toAdd.getBackground());
        values.put(SQL.COLUMN_RELEASE, toAdd.getReleaseDate());
        long insertId = database.insert(SQL.TABLE_FAVORITES, null,
                values);
        Cursor cursor = database.query(SQL.TABLE_FAVORITES,
                allColumns, SQL.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Film newFilm = cursorToFilm(cursor);
        cursor.close();

        close();

        return newFilm;
    }

    public void deleteFilm(Film film) {

        open();

        long id = film.getId();
        if (BuildConfig.LOGGING)
            Log.i("", "Film deleted with id: " + id);
        database.delete(SQL.TABLE_FAVORITES, SQL.COLUMN_ID
                + " = " + id, null);

        close();
    }

    public boolean containsFilm(Film film) {

        open();

        String Query = "Select * from " + SQL.TABLE_FAVORITES + " where " + SQL.COLUMN_ID + " = " + film.getId();
        Cursor cursor = database.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            close();

            return false;
        }
        cursor.close();

        close();

        return true;
    }

    public ArrayList<Film> getAllFilms() {

        open();

        ArrayList<Film> films = new ArrayList<Film>();
        Cursor cursor = database.query(SQL.TABLE_FAVORITES,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Film film = FilmDataSource.cursorToFilm(cursor);
            films.add(film);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();


        return films;
    }

    public static Film cursorToFilm(Cursor cursor) {
        Film film = new Film(
                cursor.getLong(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getLong(4));
        return film;
    }
}
