package cz.muni.fi.pv256.movio.uco359685.adapter;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.muni.fi.pv256.movio.uco359685.BuildConfig;
import cz.muni.fi.pv256.movio.uco359685.Constants;
import cz.muni.fi.pv256.movio.uco359685.MainActivity;
import cz.muni.fi.pv256.movio.uco359685.R;
import cz.muni.fi.pv256.movio.uco359685.model.Film;
import cz.muni.fi.pv256.movio.uco359685.model.Singleton;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Martin on 11/19/2015.
 */
public class FetchDataIntentService extends IntentService {
    public static final String PARAM_IN_MSG = "imsg";
    public static final String PARAM_OUT_MSG = "omsg";

    public FetchDataIntentService() {
        super("FetchDataIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
//      receiving parameters
//        String msg = intent.getStringExtra(PARAM_IN_MSG);

        if(BuildConfig.LOGGING)
            Log.i("", "som v intente");

        try {
            Gson gson = new GsonBuilder()
                .registerTypeAdapter(Long.class, new JsonDeserializer<Long>() {
                    @Override
                    public Long deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            Date createDate = formatter.parse(json.getAsString());
                            return createDate.getTime();
                        } catch (java.text.ParseException e) {
                            throw new JsonParseException(e.getMessage());
                        }
                    }
                })
                .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            RetrofitFilmService retrofitAPI = retrofit.create(RetrofitFilmService.class);

            //prepare headers
            Singleton.Header popularHeader = Singleton.getHeader(getResources().getString(R.string.popular));
            Singleton.Header kidsHeader = Singleton.getHeader(getResources().getString(R.string.kids));
            ArrayList<Film> allMovies = new ArrayList<Film>();

            //popular movies
            ArrayList<Film> pMovies = retrofitAPI.getPopularMovies().execute().body().results;
            if (pMovies != null) {
                popularHeader.Count = pMovies.size();
                allMovies.addAll(pMovies);
            } else
                popularHeader.Count = 0;
            //popular kids movies
            ArrayList<Film> kMovies = retrofitAPI.getKidsMovies().execute().body().results;
            if (kMovies != null) {
                kidsHeader.Count = kMovies.size();
                allMovies.addAll(kMovies);
            } else
                kidsHeader.Count = 0;

            Singleton.getInstance().setFilmList(allMovies);

            //set headers
            ArrayList<Singleton.Header> headerList = new ArrayList<Singleton.Header>();
            headerList.add(popularHeader);
            headerList.add(kidsHeader);
            Singleton.getInstance().setHeaderList(headerList);
        }
        catch (Exception e) {

            Singleton.getInstance().setFilmList(new ArrayList<Film>());
            Singleton.getInstance().setHeaderList(new ArrayList<Singleton.Header>());

            if(BuildConfig.LOGGING)
                Log.i("", e.toString());

            Intent nIntent = new Intent(this, MainActivity.class);
            PendingIntent pIntent = PendingIntent.getActivity(this, 0, nIntent, 0);

            Notification n  = new Notification.Builder(this)
                    .setContentTitle(getResources().getString(R.string.notify_error_header))
                    .setContentText(getResources().getString(R.string.notify_error_msg))
                    .setSmallIcon(R.drawable.ic_add_black_24dp)
                    .setContentIntent(pIntent) //přesměruje nás do aplikace
                    .setAutoCancel(true)
                    .build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(0, n); //0 udává číslo notifikace. Na některých zařízeních nefunguje jinačí int než 0.
        }

        if(BuildConfig.LOGGING)
            Log.i("", "idem broadcastit " + Singleton.getInstance().getFilmList().size());

        // processing done here….
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(MainActivity.ResponseReceiver.ACTION_RESP);
        broadcastIntent.setClass(this, MainActivity.ResponseReceiver.class);
//        broadcastIntent.putExtra(PARAM_OUT_MSG, "data");
        sendBroadcast(broadcastIntent);
    }
}
