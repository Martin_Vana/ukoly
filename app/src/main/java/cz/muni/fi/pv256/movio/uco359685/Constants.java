package cz.muni.fi.pv256.movio.uco359685;

/**
 * Created by Martin on 10/10/2015.
 */
public class Constants {
    public static final String API_KEY = "&api_key=9e98abd8cdc542251a4e342a147374b6";
    public static final String API_KEY_ALT = "?api_key=9e98abd8cdc542251a4e342a147374b6";

    public static final String MOVIE_URL = "movie/{id}";
    public static final String POPULAR_URL = "discover/movie?sort_by=popularity.desc&page=1";
    public static final String KIDS_URL = "discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc";

    public static final String BASE_URL = "http://api.themoviedb.org/3/";
    public static final String BASE_POSTER_URL = "http://image.tmdb.org/t/p/w342";
    public static final String BASE_BACKDROP_URL = "http://image.tmdb.org/t/p/w780";
}
